<?php

namespace Login\LoginBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Login\LoginBundle\Entity\Users;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        if($request->getMethod()=='POST') {
            $username = $request->get('username');
            $password = $request->get('password');

            $em = $this->getDoctrine()->getEntityManager();
            $repository = $em->getRepository('LoginLoginBundle:Users');

            $user = $repository->findOneBy(array('userName'=>$username, 'password'=>$password));
            if ($user) {
                return $this->render('LoginLoginBundle:Default:login.html.twig', array('name' => $user->getFirstName()));
            }
        } else {
	    	return $this->render('LoginLoginBundle:Default:login.html.twig');
	    }
    }
    public function signupAction(Request $request) 
    {
        if($request->getMethod()=='POST') {
            $username = $request->get('username');
            $firstname = $request->get('firstname');
            $password = $request->get('password');

            $user = new Users();
            $user->setUserName($username);
            $user->setFirstName($firstname);
            $user->setPassword($password);
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($user);
            $em->flush();

        }
        return $this->render('LoginLoginBundle:Default:signup.html.twig');
    }
}
